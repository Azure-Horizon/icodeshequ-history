﻿# 第2节：抄袭者时期时的知名工作室

## 抄袭者时期时的知名工作室
染砸工作室最早的作品可以追溯至2020年1月5日，创始人和现任室长为@染砸佩恩，其本人现在已经高中，当时和冬瓜工作室保持友好关系，和冬瓜工作室一样是做UT类的作品，染砸工作室建立的小图灵交流群里聚集了众多当时在小图灵有名的人物。

[秦氏工作室](https://icodeshequ.youdao.com/work/42b70e52eaf53fbc8ff2a4ee8cd111c7?from=discovery)，英文名YDJ Studio，成立于2020年1月25日,是有道小图灵现存最老的工作室，拥有较大影响力。它的成立引起了有道小图灵史上第一次工作室热潮，目前专注于制作Scratch游戏，总部位于有道小图灵，在A营与共创世界都有分部，早先在国外的scratch国际官网也有分部，创始人为[@John](https://icodeshequ.youdao.com/personal?userId=d3hvWFFVRGpfWkt1ZHRVVUtBR3RfbFZ1MnhMbFJF)（即现任室长）。

N编辑工作室于2020年2月10日在卡搭社区成立，2020年2月12日开始在有道小图灵招人，并于同日建立QQ群，制作工作室室标等。N编辑工作室主要用scratch做动画、模拟器等。其成员有scratch、Python、gamemaker8、开源电子EV3等方面的学习经验，以及二维码制作，还会硬件编辑——但是缺乏硬件。主要活跃于总部网易有道卡搭社区，但自从卡搭崩溃以来主要转移到了有道小图灵社区。也活跃于A营。现任室长为其创始人[@N编辑工作室](https://icodeshequ.youdao.com/personal?userId=d3hvWFFVRGowdXZHeEtWSEo1SG1heDZhbzJHMkJ3)。

冬瓜工作室成立于2020年3月5日，创始人是[@蒋冬杉](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuOTc4MjkxMGFiYTU4NDc3ZThAMTYzLmNvbQ%3D%3D)。该工作室主要以制作scratch游戏为主，其做出了有道小图灵首款剧情化游戏[《亮亮的奇妙之旅》](https://icodeshequ.youdao.com/work/06acb619328724bf211ff500afab620e?from=discovery)，开阔了有道小图灵游戏的视野。冬瓜工作室主要在有道小图灵活跃，在A稽和卡塔早先也都有涉猎。可惜的是，@蒋冬杉因学业问题逐渐减少上线时间，使得工作室活跃成员越来越少，最终冬瓜工作室解散，@蒋冬杉加入秦氏工作室（摸鱼），现在是秦氏工作室名誉成员。

NH星辰工作室于2020年早期由NH与晨光联合创立，其招人作品目前最早追溯于2020年5月12日，其成员中出现过许多大佬和优质作品，@steve和@画渣末水都来自这个工作室，后来大部分人退站，室长失踪，NH也随之解散。

2020年5月17日，清风工作室成立，其创始人和室长是@某科学的歌乱舞，以制作剧情化游戏为主。主要在有道小图灵活跃，在A稽社区和卡搭网站也有分布。该工作室和冬瓜工作室比较亲近，都喜欢有关传说之下的内容。目前消亡。