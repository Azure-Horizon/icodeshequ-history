﻿#第1节：抄袭三恶人与铭文工作室

##抄袭者时期与抄袭三恶人

<figure style="float: left;">
    <img src="猫哥露脸照.svg" alt="图片">
    <figcaption>猫哥露脸照</figcaption>
</figure>

抄袭者时期在2019年11月由[@末城](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuNjQzM2U4N2NiZWJjNDgyYThAMTYzLmNvbQ%3D%3D)和[@猫哥](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuNjVjMDNjY2JmZTU0NGQzM2FAMTYzLmNvbQ%3D%3D)两大最臭名昭著的抄袭者所开创，他们和后来的另一知名抄袭者[@007-小柴犬-鸿铭](https://icodeshequ.youdao.com/personal?userId=d3hvWFFVRGo2MHl4WlA3SlhaaUJSU3LVNlRmY4)被共同称作"有道小图灵抄袭三恶人"。@末城、@猫哥和后来的@007-小柴犬-鸿铭利用当时人们对于外网所知甚少，对于抄袭概念不知道不理解的心理，以及当时社区举报系统不完善的问题，通过技术手段从外网大批量抄袭Scratch好作，并冒充他们自己原创的作品，甚至堂而皇之地标明所谓“原创声明”，靠这种严重侵害原作者版权的手段骗得了巨量的点赞，他们的许多抄袭作品在当时几乎霸占点赞榜前20，@末城、@猫哥和@007-小柴犬-鸿铭也成了当时红得发紫的“Scratch超级大佬”。其中@007-小柴犬-鸿铭后来还成立了铭文工作室，并使用招募出名抄袭成员等手段使铭文工作室迅速出名，甚至成为铭文工作室室员在当时都成为一种荣耀。

<figure style="float: right;">
    <img src="末城露脸照.svg" alt="图片">
    <figcaption>末城露脸照</figcaption>
</figure>

有道小图灵抄袭三恶人的这种行为对当时的小图灵产生了重大影响，他们的抄袭行为不仅破坏了当时的社区Scratch良好创作氛围，成为了小图灵抄袭之风的先河，而且还使他们成为全小图灵网站最臭名远扬的抄袭者，被后人所唾弃。
 
 
##铭文工作室


[铭文工作室](https://icodeshequ.youdao.com/work/4e0c257dfda0cfc7bea03bc8a4f04bd3?from=home)成立于2020年3月18日，室长是@007-小柴犬-鸿铭。铭文工作室对外宣称是个正常工作室并欢迎他人加入，但其实际上为有道小图灵最臭名昭著的抄袭者组织，在当时具有十分大的影响力，"铭文工作室时
期"也因此成为了抄袭者时期的别名。铭文工作室巅峰时有24名成员，其中许多成员包括室长@007-小柴犬-鸿铭都是不折不扣的抄袭者，@末城后来加入铭文工作室，并改名为“001-MX灬末城”，他的加入不仅使铭文工作室更加迅速的成为当时“荣耀的象征”，还是其最终覆灭的一个重要原因。

<figure style="text-align: center;">
    <img src="铭文工作室招生作品.svg" alt="图片">
    <figcaption>铭文工作室招生作品</figcaption>
</figure>

铭文工作室后来受到FCX反抄袭组织和OP基金会正义的揭发，被全网站针对，于2020年8月左右崩溃。该工作室虽未官宣解散,但基于主要成员都已退站的状态,可以视为解散,不过仍有部分剩余成员以及伪残党活动，最典型的案例：@铭文-512-跑酷凋零神，@铭文工作室-鬼才怪人，2个未曾加入铭文，却宣称自己是铭文工作室的奇葩抄袭者,仍旧摇着铭文的大旗。铭文工作室算是最成功的抄袭工作室，但是这个工作室也诞生过一些原创作品大佬，@坚仔，@残留的悲伤_萌新，@达达等小图灵后起之秀都曾经加入过铭文工作室。
