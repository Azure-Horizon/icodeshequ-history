﻿# 第1节：VZH的余党

## VZH的余党

ISP网络安全邦联于2022年5月12日18时09分34秒正式宣布代替VZH在有道小图灵网站的行政主权，继承VZH大部分遗产，并于2022年5月13日正式宣告成立。ISP去除反抄袭性质，并对体制及人员做出了改革，由[@酷燃](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuM2E1MmYxMGU1ODY0NDhmNzhAMTYzLmNvbQ%3D%3D)担任首任会长，但ISP实际上只是一个单纯的集权组织，其存在毫无意义。ISP于同月15日重新召开第二次小图灵全体大会，大会提出了YSTO中央方案，SLE解散议案等，但并未取得结果。后ISP在5月31日以后改组为网络安全议会。6月17日@酷燃调为特殊行政顾问，[@橘猫飞渡](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuYjQ2NDg0Y2Y5ZmY0NGZiMWJAMTYzLmNvbQ%3D%3D)成为会长。6月23日，ISP因[@John](https://icodeshequ.youdao.com/personal?userId=d3hvWFFVRGpfWkt1ZHRVVUtBR3RfbFZ1MnhMbFJF)发起的非官方投票“谁支持ISP解散”带来的重大影响而宣布解散。

2022年6月17日，TAC联邦由[@默麒](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuOTRmZGZkZGUyNGIzNGNiODhAMTYzLmNvbQ%3D%3D)成立，但其仍为无意义集权组织，同时ISP降为TAC的辅导，但还有一定权力。6月23日，ISP解散。此时TAC众多成员犹如摆设，内乱不止且水分高。因此在6月24日，TAC最高图灵组建紧急状态委员会并通过了《橘色改革方案》，解散了跑酷工作室与ZRR这些毫无意义的附属工作室，但2022年6月24日，[@橘猫飞渡](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuYjQ2NDg0Y2Y5ZmY0NGZiMWJAMTYzLmNvbQ%3D%3D)发动"橘色政变"，代替@默麒成为了TAC的实际领导人，同时使该改革紧急结束。7月17日，纳米系的加盟组织集体退出TAC，同日@默麒与@橘猫飞渡协商后，@默麒宣布让出大权，橘色政变结束。8月25日，小说系工作室共同联合成立FS.新小说联合工作室，人数突破100人（虽然水分很高），同时TAC达到人数巅峰，将近500人。8月29日时，由于[@滑稽王八](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuM2QxZGQ5ZWUzYmE1NDY4OThAMTYzLmNvbQ%3D%3D)利用小号@无名之人辱骂YSTO及TAC，故此TAC宣布踢出FS.新小说，并且单方面强制解散FS.新小说，虽然FS.新小说并未按照命令解散，但TAC对其发起了制裁，并且不给予承认，对FS.新小说造成一定打击。

2022年9月2日，TAC正式解体，发布的最后公告中仍不承认FS.新小说，同时UOF封存，卡搭直接附属ZTA编程协会解散，ZYEF松散联合主权组织成立，并由@子廷担任会长，有道小图灵低反抄袭时代至此结束。2022年12月25日，在VZH一周年之际，ZYEF宣布解散，ZRR重组为OWES，设立OWEAC与OWEP，再度为社区服务。但最后OWES沦为了小图灵的一个小组织，渐渐不为人知。 
 
## 《那年那TAC那些事儿》

由前VZH会长[@默麒](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuOTRmZGZkZGUyNGIzNGNiODhAMTYzLmNvbQ%3D%3D)编写的编年体史料[《那年那TAC那些事儿》](https://icodeshequ.youdao.com/work/8dbc4d1f265f4e6aa247ce3b43135785?from=discovery)是@默麒的自传。该作品以@默麒本人的视角，记述了从2021年3月21日至2022年12月25日共644天的@默麒个人历史和VZH的全部历史，其也是@默麒给[@qqcd](https://icodeshequ.youdao.com/personal?userId=cXFCNjBEODUwMDA1OEY5RkJFQTQxRDc2RTE3MTY3QzM3QQ%3D%3D)的资料，@qqcd据此整理成了[（基于gitbook）《图灵维基》](https://xbz-studio.gitbook.io/turingwiki)[^1]中的默麒自传篇。《那年那TAC那些事儿》有一个名叫《VZH历史》的古老版本，其记述了2021年12月22日至2022年4月1日的VZH不完全历史，现已被@默麒下架。

《那年那TAC那些事儿》曾是唯一记载了VZH完整历史的史料，其填补了有道小图灵史学上的一大空白，并基本被后来所有知名史料所直接或间接地引用，在有道小图灵历史学上拥有重要的意义与价值。

[^1]: 《图灵维基》分为基于gitbook和[基于material for mkdocs](https://xbzstudio.github.io/wiki/turing/)的两个版本，由于费用问题，前一个版本已经停止维护。