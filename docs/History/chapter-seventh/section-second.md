﻿# 521HTML事件及其后续发展

<figure style="float: right; margin-left: 15px;">
    <img src="《紧急通知！你的作品要没了！》截图.svg" alt="图片">
    <figcaption>《紧急通知！你的作品要没了！》截图</figcaption>
</figure>

2022年5月，黑客@是冰梨呀进入有道小图灵，他偶然间发现了有道小图灵网站存在的一个严重的漏洞：通过在评论区发布特定代码，可以将作品页面重定向到有道卡搭主页（实际上可以是任何页面），导致受影响的作品在该问题存在时将永远无法正常打开。然而，@冰梨并未向官方报告这一漏洞，反而在2022年5月21日开始恶意利用该漏洞攻击一些作品，甚至发布公告披露该漏洞代码，并最终导致漏洞代码泄露。521HTML事件（也称为HTML代码事件）至此爆发。以[@朱国维](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuZDA4NmI2NDYxNTU2NDdkNThAMTYzLmNvbQ%3D%3D)、[@sb一个](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuZTUxNTY0ZjI4ZjcxNGY1MDhAMTYzLmNvbQ%3D%3D)（真名周晨宇）为首的许多人纷纷效仿，疯狂地利用漏洞代码对其他作品进行攻击，导致大量作品被重定向，甚至[@John](https://icodeshequ.youdao.com/personal?userId=d3hvWFFVRGpfWkt1ZHRVVUtBR3RfbFZ1MnhMbFJF)的所有作品都遭受攻击。[@qqcd](https://icodeshequ.youdao.com/personal?userId=cXFCNjBEODUwMDA1OEY5RkJFQTQxRDc2RTE3MTY3QzM3QQ%3D%3D)对@冰梨的行为感到过分，因此发布了紧急通知《紧急通知！你的作品可能会消失！》，呼吁所有人暂停发布重要作品，但仍有许多人受到影响。短短几个小时内，有道小图灵网站陷入了史上首次波及全网的大恐慌，@qqcd的通知迅速传播，几乎刷爆了屏幕，许多作品被取消发布，大量慌乱的受害者选择报警。随后，官方于5月21日当晚紧急修复了漏洞，521HTML事件得以结束，大范围的恐慌也逐渐平息。

尽管521HTML事件已经结束，但是其带来的影响并未消失，反而变得更加严重。@是冰梨啊换了账号，继续进行黑图灵活动，而@冰梨的妹妹@是诗诗啊（真名王诗雨）也加入了这个社区。同时，由于“对技术的追求”，@qqcd并没有谴责@冰梨的行为，反而支持他们。@冰梨兄妹和@qqcd合作利用漏洞进行破坏活动。@冰梨利用小图灵Python自带的urllib库开发出了可以自动点赞、收藏、评论、刷浏览量和修改个人签名的python工具代码，这些代码一经发布就广泛传播，造成人们防不胜防，许多人通过这些工具获取利益。@冰梨等人还多次对小图灵网站发起攻击，导致社区长时间瘫痪，首页和发现页频繁崩溃，其中数次崩社区的时长足足达数个小时。[@qqcd](https://icodeshequ.youdao.com/personal?userId=cXFCNjBEODUwMDA1OEY5RkJFQTQxRDc2RTE3MTY3QzM3QQ%3D%3D)学习了@冰梨的做法，借助有道小图灵scratch版本低的因素，在scratch中植入了注入html代码的svg图片，实现了scratch作品内跳转网页的效果。这段社区安全面临危机、社区秩序极度混乱的时期，也因此得名"521后危机混乱期"。

<figure style="float: left; margin-right: 15px;">
    <img src="冰梨制作的网站.svg" alt="图片">
    <figcaption>冰梨制作的网站</figcaption>
</figure>

在521后危机混乱期之中，社区处于极度混乱状态，官方因漏洞修复不及时而备受指责，@冰梨等人的犯罪行为变得更加肆无忌惮。直到2022年8月21日，有道小图灵官方宣布将收集法律证据，对@冰梨和[@qqcd](https://icodeshequ.youdao.com/personal?userId=cXFCNjBEODUwMDA1OEY5RkJFQTQxRDc2RTE3MTY3QzM3QQ%3D%3D)采取制裁措施。@冰梨匆忙逃往卡搭，@qqcd则注册新账号进入养老状态。官方又于9月中旬删除了urllib等库，使得python工具代码在小图灵网站失去了生存空间。然而，人们仍然可以通过线下操作为作品刷大量浏览量，使其登上首页。直至目前官方仍未解决这一问题，精选推荐栏和人气周榜栏的可靠性也持续受到挑战。