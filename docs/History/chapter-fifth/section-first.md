﻿# 第1节：VZH零式无常反抄袭联合联盟

## VZH零式无常反抄袭联合联盟

<figure style="float: right;">
    <img src="VZH图标.svg" alt="图片">
    <figcaption>VZH的图标</figcaption>
</figure>

VZH零式无常反抄袭联合联盟又称VZH反抄袭联合联盟，并无正式英文名，简称VZH（指VZH的最初三大加盟组织：VCU+ZRR+HWM）,其成立于2021年12月23日，解散于2022年5月12日，历经140天。VZH是一个单一的反抄袭联盟制组织，对于反抄袭有着极大的贡献。
 
2021年12月22号20:37，ZRR反抄袭突击队创始人[@默龙](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuOTRmZGZkZGUyNGIzNGNiODhAMTYzLmNvbQ%3D%3D)为求加大反抄袭力度，向当时ZRR的战略合作伙伴VCU室长@9360-白鹤提出合并，以及巩固VCU与ZRR的合作关系。后于2021年12月23号21:30，VCU会长及创始人@白鹤同意，并签署《ZRR VCU合并条约》，这标志着VZH（当时初名ZRRVCU）的正式建立，其迅即成为了当时有道小图灵第四大的反抄袭组织。@默龙是VZH第一任会长。

2021年12月24号晚上7点，寒梦工作室HMW（HANMENG WORKROOM，非反抄袭工作室）作为VCU的合作伙伴加入，VZH突破100大关，成为小图灵第二个突破100人的组织和网站第二大组织。

2021年12月24号晚上10点，VZH高层第一次大会召开，参加会议领导为：[@默龙](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuOTRmZGZkZGUyNGIzNGNiODhAMTYzLmNvbQ%3D%3D)，@白鹤，@时七，@寒冬，此次会议被VZH内部称为“夜幕会议”，其内容确认了VZH未来的发展方向和“坚定歼灭抄袭”的中央政策，规划了VZH的战略方针以及对于AOC的战略，有着里程碑式的意义。

2021年12月27日，VZH第二任会长@默犼（即@默龙）即位。后于2022年1月1日，@默犼换号为@默麒并再度即位，成为VZH第三任会长。@默麒推翻了他自己以@默犼的身份所领导的保守派，并开启了VZH第一次改革，在改革中@默麒领导的进步派上位并大力发展VZH。这次改革史称“默麒改革”，“默麒改革”是VZH的第一个发展期，其改革了VZH准则体系，中央方向和民主制度。在这段期间，VZH为社区做出大量贡献，默麒改革的开始也标志着群雄时期的结束与VZH时代的正式来临，在此之后，VZH逐渐成为有道小图灵社区反抄袭势力的领导组织。2022年1月17号晚上8点26分，VZH早期加盟组织VCU瓦解，并入ZRR，这使的ZRR实际上基本掌控了VZH。

2月2号，@默麒开启了继承人大选，参加人员有：[@科尼赛克](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuNThiNzI3OTNjMDRiNDQ2NDhAMTYzLmNvbQ%3D%3D)，[@有道J-游戏一生](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuYTRkOTNlMjU1NjcwNDNhOGFAMTYzLmNvbQ%3D%3D)，[@李岱乐](https://icodeshequ.youdao.com/personal?userId=d3hvWFFVRGoyNVlTVGhnLXJZOE9GUVJ6aXVMZmpn)和@柠檬味的夏天。最终@有道J-游戏一生胜出，并于2月17日即位，成为了VZH第四任会长。@游戏一生也属于进步派，其上台后进行大量改革，并启动成员清洗。他在位期间VZH稳定太平，历史上称为“游生之鼎”。

2月底，HMW解体，并入XAA组织。至此VZH彻底被ZRR所掌控。3月6日，APM（阿法工作室）加入，VZH突破150人大关，超越AOC，成为当时全有道小图灵最大组织。同月28日，DSH&TDS联合工作室加入VZH，其加入标志着VZH达到其鼎盛期，成员数共增加到254名。

4月1日，@游戏一生暂时退站，成为荣誉成员。特殊行政顾问[@潘西](https://icodeshequ.youdao.com/personal?userId=d3hvWFFVRGo3RUFjMHg1TV85dUdTQjdHa0dlR0ZVXzI%3D)成为第5任会长，她是第一位VZH女领导人，上位后做出较大改革，并曾撰写《论小图灵的抄袭与喷子》一文，被定为VZH官方论文。4月17日，@潘西退站，[@一冉yaya](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuMGI3NzJjM2FiYjFlNDljOWJAMTYzLmNvbQ%3D%3D)紧急成为特殊行政会长（第6任）。4月28日，[@酷燃](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuM2E1MmYxMGU1ODY0NDhmNzhAMTYzLmNvbQ%3D%3D)当选VZH第7任会长，后VZH于5月10日召开第二次小图灵全体大会，但由于512网警事件的爆发而推迟。2022年5月12日，因512网警事件，VZH宣布解体，持续共131天的VZH时代结束，此后，再也没有领导整个时代的反抄袭组织出现。

VZH历代会长：

* 第一任：[@默龙](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuOTRmZGZkZGUyNGIzNGNiODhAMTYzLmNvbQ%3D%3D)
* 第二任：[@默犼](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuOTRmZGZkZGUyNGIzNGNiODhAMTYzLmNvbQ%3D%3D)
* 第三任：[@默麒](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuOTRmZGZkZGUyNGIzNGNiODhAMTYzLmNvbQ%3D%3D)
* 第四任：[@有道J-游戏一生](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuYTRkOTNlMjU1NjcwNDNhOGFAMTYzLmNvbQ%3D%3D)
* 第五任：[@潘西](https://icodeshequ.youdao.com/personal?userId=d3hvWFFVRGo3RUFjMHg1TV85dUdTQjdHa0dlR0ZVXzI%3D)
* 第六任：[@一冉yaya（特殊行政会长）](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuMGI3NzJjM2FiYjFlNDljOWJAMTYzLmNvbQ%3D%3D)
* 第七任：[@酷燃](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuM2E1MmYxMGU1ODY0NDhmNzhAMTYzLmNvbQ%3D%3D)
