﻿# 第2节：512网警事件

## 512网警事件
2022年5月11日，VZH时代的知名反抄袭组织CPA的会长[@qqcd](https://icodeshequ.youdao.com/personal?userId=cXFCNjBEODUwMDA1OEY5RkJFQTQxRDc2RTE3MTY3QzM3QQ%3D%3D)在接单画作时，遭到一个叫做“章北海”的人无端指责画作质量不佳。由于@qqcd与[@章北海](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuODhhOTM0MmZlMWE1NDI3YThAMTYzLmNvbQ%3D%3D)之间存在恩怨，双方立即展开激烈争执。这场微小的口角逐渐升级演变，成为了512网警事件（也称为512章北海事件）的开端。

最终，为宣泄愤怒，@qqcd将章北海列入OP公告（当时OP与CPA合并），号召人们对章北海展开围攻，手段包括网暴、举报和宣传。@qqcd发布的OP公告要求OP及其合作协会成员（如VZH）必须参与攻击或支持对章北海的行动，并坚定地置于封面上。很快，大批人支持了@qqcd，不到几小时，章北海就陷入了网络暴力的困境。

[@qqcd](https://icodeshequ.youdao.com/personal?userId=cXFCNjBEODUwMDA1OEY5RkJFQTQxRDc2RTE3MTY3QzM3QQ%3D%3D)甚至采用了恶毒的开放改编权限方式，并在简介中明确介绍，导致OP公告在发现页面频繁刷屏，引人注目。同时，@qqcd与众人一起，自己操作着三个账号，强制将[章北海](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuODhhOTM0MmZlMWE1NDI3YThAMTYzLmNvbQ%3D%3D)的代表作[《章北海跑酷》](https://icodeshequ.youdao.com/work/70cb2139af844000bf1539b73858254b?from=discovery)塞满了六千条辱骂评论，创下了图灵社区的历史最高记录。

@qqcd原以为胜券在握，等待着章北海的道歉。然而，聪明的章北海没有屈服于虚伪的反抄袭组织，而是选择向网信办举报了@qqcd等人的恶行。网信办接到举报后，对有道公司发出命令，下令有道公司删除所有针对章北海不利、侵犯其尊严的作品和评论，并永久封禁@qqcd的账号，清除其所有作品。

5月12日上午，[官方](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuOGE2MmNmN2I2M2I4NDNlZGFAMTYzLmNvbQ%3D%3D)开始出面劝阻@qqcd及其施暴者，并下架了许多类似OP公告的作品，尽管@qqcd仍冒险让许多人坚持重新发布。但大多数人看到官方下场，已经开始主动撤退，以免惹祸上身。比如当时的VZH前会长@默麒发布作品，向章北海致歉，并表示“意识到自己的错误”。而@qqcd等人坚持刷了十余页后，@qqcd宣布停止进攻，观察形势。

当天中午，小图灵网站首页突然发布了一则公告[《给有道小图灵小小创作家们的一封信》](https://shimo.im/docs/e1Az4E5zGytloJqW/read)，呼吁反对网暴式反抄袭，倡导通过举报来进行反抄袭（低反抄袭）。官方还在当日下午执行了网信办的封号、删除评论等命令。慌乱中的@qqcd向[章北海](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuODhhOTM0MmZlMWE1NDI3YThAMTYzLmNvbQ%3D%3D)致歉，并公开了他的小号@有道小图灵精品教程中心的账号密码和他自己的姓名（张翊辰大帅逼），承认自己的过失。与此同时，参与512网警事件的反抄袭组织（VZH、CPA、OP等）因涉嫌违法宣布解体，其他反抄袭组织也受到牵连，纷纷转型为普通工作室、低反抄袭组织或解散。

<figure style="text-align: center;">
    <img src="《给有道小图灵小小创作家们的一封信》局部截图.svg" alt="图片">
    <figcaption>《给有道小图灵小小创作家们的一封信》局部截图</figcaption>
</figure>

512网警事件结束了长达680天的反抄袭时代，对当时有道小图灵繁荣的反抄袭体系造成了致命打击。其彻底清除了当时深入人心的网暴式反抄袭思想，成为了有道小图灵低反抄袭时代的开端，是有道小图灵历史上影响最重大、最深远的事件之一。


### 相关史事
在[《听听大家的心声来评论吧》](https://icodeshequ.youdao.com/work/4c0dae24c0e24c6897c051ded9f0c5d9)中，[官方](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuOGE2MmNmN2I2M2I4NDNlZGFAMTYzLmNvbQ%3D%3D)引用了[@萌萌梦幻神兽](https://icodeshequ.youdao.com/personal?userId=cXFCNjBEODUwMDA1OEY5RkJFQTQxRDc2RTE3MTY3QzM3QQ%3D%3D)的举报案例作为良好举报的示例。然而讽刺的是，@萌萌梦幻神兽正是512网警事件的始作俑者@qqcd本人的大号，这也成为了有道小图灵颇具讽刺意味的笑料之一。