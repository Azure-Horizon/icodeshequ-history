﻿# 第3节：詹姆斯事件和坚仔事件

## 詹姆斯事件和坚仔事件

詹姆斯事件是坚仔事件的前传，其发生于2022年3月25日。起因是[@詹姆斯](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuOTU3ODYwMzEzZGEyNDdmMWFAMTYzLmNvbQ%3D%3D)的[作品](https://icodeshequ.youdao.com/work/6991a41d72dc46d1b533d2e30c433366?from=discovery)抄袭了百度上的Python作品，拥有几十个赞但并未标明原创。被反抄袭组织发现并以网暴类方式初步处理后因为此人“极厚的脸皮和高超的删评技术”，被更多人所针对，评论量甚至被刷到了3千。后来@詹姆斯标明转载，反抄袭组织的成员们消停，@詹姆斯上完课淡退，此事件结束。

真正重要的是詹姆斯事件的衍生事件——坚仔事件。它是小图灵历史上最后一个大规模网暴刷屏事件。姆斯事件期间，有一个人名为“支持詹姆斯”，并不断发布支持[@詹姆斯](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuOTU3ODYwMzEzZGEyNDdmMWFAMTYzLmNvbQ%3D%3D)的言论和作品，被许多人所骂。后来此人自曝是当时高赞大佬@坚仔的小号，以CPA为首的反抄袭组织因此将矛头指向了@坚仔。当天，CPA会长[@qqcd](https://icodeshequ.youdao.com/personal?userId=cXFCNjBEODUwMDA1OEY5RkJFQTQxRDc2RTE3MTY3QzM3QQ%3D%3D)与众人在classin群“干死詹姆斯”中，开始策划对@坚仔的攻击。参与事件的人有@qqcd、@麻薯、[@豆奶](https://icodeshequ.youdao.com/personal?userId=cXE0RTE1QkM5RjVBRDEwNDUwQjczMkZBNkRFQjA2NTIwQQ%3D%3D)、@舟洪楷、[@一冉yaya](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuMGI3NzJjM2FiYjFlNDljOWJAMTYzLmNvbQ%3D%3D)等众多人。为攻击@坚仔的行动进行了周密的策划。晚上，对@坚仔的攻击正式发动。在大规模刷屏、网络暴力的行动过程中，@坚仔曾不止一次出来表示那个人不是他，但最后没有劝阻成功，最后自闭。

在实施攻击至300余评论后，@支持詹姆斯加入了该classin群聊，并表明自己不是@坚仔，是@坚仔的同学，他只
是借反抄袭组织之手整了整@坚仔，@坚仔已经哭了。起初众人并不相信，因为类似的戏码实在太多。但是当众人对其进行深入沟通，并伪造身份套取信息时，发现这确是事实。且在套取信息时，“支持詹姆斯”表示自己已经不愿意再对@坚仔进行整蛊，因为他害怕@坚仔回到学校后告诉老师，整蛊@坚仔也只是因为其在课上让自己出丑，想要报复而已。

最后，各反抄袭协会向@坚仔道歉，@坚仔隐退，“干死詹姆斯”群改名为“会放屁的蘑菇”，@qqcd和众人删除大部分刷出的评论，坚仔事件告终。

“科尼赛克事件” 是坚仔事件的衍生事件。[@科尼赛克](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuNThiNzI3OTNjMDRiNDQ2NDhAMTYzLmNvbQ%3D%3D)想为@坚仔报仇，在微信群招募有志之士对抗[@qqcd](https://icodeshequ.youdao.com/personal?userId=cXFCNjBEODUwMDA1OEY5RkJFQTQxRDc2RTE3MTY3QzM3QQ%3D%3D)，被[@John](https://icodeshequ.youdao.com/personal?userId=d3hvWFFVRGpfWkt1ZHRVVUtBR3RfbFZ1MnhMbFJF)透露给@qqcd后，@qqcd与@科尼赛克产生矛盾，@科尼赛克名字被挂上@qqcd的作品并被@qqcd大肆攻击，在身名败裂后@科尼赛克假装被盗号，说出类似于“末城，我来给你报仇啦！”的奇妙言论，最后与@qqcd、@John成为仇家。