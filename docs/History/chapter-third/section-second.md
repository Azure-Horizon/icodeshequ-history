﻿# 第2节：有道小图灵反抄袭革命

## 有道小图灵反抄袭革命
2020年6月31日，FCX升为反抄袭协会，正式开始对铭文工作室、@001-MX灬末城和@猫哥进行正义的讨伐。FCX方先后发布[《FCX新闻》](https://icodeshequ.youdao.com/work/f7db5563dc03493f9414e809d75be892?from=discovery)和《讨伐猫哥》，分别对[铭文工作室](https://icodeshequ.youdao.com/work/4e0c257dfda0cfc7bea03bc8a4f04bd3?from=home)和@001-MX灬末城、@猫哥进行揭发，并通过开放改编权限等方式积极进行推广，这标志着有道小图灵反抄袭革命的正式爆发。

<figure style="float: right; margin-left: 15px;">
    <img src="《FCX新闻》截图.svg" alt="图片">
    <figcaption>《FCX新闻》截图</figcaption>
</figure>

FCX的揭发作品一经发布，便迅速轰动全网。大量有识之士通过揭发作品明白了@001-MX灬末城和[@猫哥](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuNjVjMDNjY2JmZTU0NGQzM2FAMTYzLmNvbQ%3D%3D)的抄袭真相及铭文工作室的老底后义愤填膺，纷纷选择通过改编并发布等方式积极转发。这导致揭发作品迅速十传百，百传千，最后形成空前绝后的刷屏浪潮，高峰期甚至随便点开作品集一页，都能看到满屏都是揭发作品，正常作品根本没有或只有一两个的奇景。而铭文工作室、[@001-MX灬末城](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuNjQzM2U4N2NiZWJjNDgyYThAMTYzLmNvbQ%3D%3D)和@猫哥的恶名则迅速人尽皆知，被全网讨伐的根本不敢冒头。他们先是不断改名，但每次都很快被识破，最终他们为逃避反抄袭革命的打击被迫选择退站，铭文工作室也主要因为@001-MX灬末城的原因而被众人针对，其成员纷纷隐匿或退站，生怕暴露自己铭文成员的身份。最终于8月左右铭文工作室正常工作室功能彻底丧失,实际上已基本崩溃。反抄袭革命的直接目标成功实现。

有道小图灵反抄袭革命终结了铭文工作室时期，是小图灵首次也是规模最大的一次反抄袭事件，它奠定了之后小图灵反抄袭的主要方式，也是有道小图灵反抄袭时代的开端，历史意义十分重大。但是这种以网暴为主的反抄袭方式，也为后来512网警事件的发生埋下了隐患。

### 历史小链接：反抄袭革命的谬误
在反抄袭革命早期，FCX和OP曾错把@001-MX灬末城误认为是铭文工作室的创建者，并将这一谬误写入了揭发作品内，这直接使得该误区随着揭发作品的广泛传播而深入人心，铭文工作室也因为其“抄袭者室长@001-MX灬末城“而受到众人针对，最终彻底崩溃。结果真正的抄袭者室长[@007-小柴犬-鸿铭](https://icodeshequ.youdao.com/personal?userId=d3hvWFFVRGo2MHl4WlA3SlhaaUJSU3ZKLVNlRmY4)竟没有受到反抄袭革命的太大波及，虽然后面渐渐有人发现了这一谬误，但其在有道小图灵仍被大多数人所相信，甚至这个误区还曾被写进了众多史料中，其也成为了小图灵迄今为止影响最深远、最重大的谬误之一。 