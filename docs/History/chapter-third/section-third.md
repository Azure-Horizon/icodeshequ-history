﻿# 第3节：OP联盟制基金会

## OP联盟制基金会
OP联盟制基金会是YSTO[^1]的前身，英文全名Oppose Plagiarism Foundation，由秦氏工作室室长[@John](https://icodeshequ.youdao.com/personal?userId=d3hvWFFVRGpfWkt1ZHRVVUtBR3RfbFZ1MnhMbFJF)于2020年7月建立，是有道小图灵第二个反抄袭组织，成立的主要目的是收集记载抄袭数据，以及记载重大事件。与FCX共同带来了小图灵第一次反抄袭巅峰, 后在第一次反抄袭大会中并入FCX并删除所有数据，后解散。2022年为了避免当时最大反抄袭组织VZH“过度军阀化”而重组，@John还打算派间谍进入VZH窃取信息，虽然当时VZH会长@默麒识破了@John的计划，但最后仍然没有找出秦氏的间谍，而且自己派去秦氏的间谍也被踢出。2022年5月12日14时57分OP基金会因为512网警事件再度解散，改为YSTO，中文名为“有道小图灵社区公约组织”，主要负责管辖和监督加盟工作室是否违反社区守则，并介入事情进行劝导和举报。

2021年，OP曾有一条隐蔽战线，是当时@John将被@猫哥抄袭过的人组成的战线联盟，在暗中搜集@猫哥抄袭的证据。不过后来战线中的成员都不干了，因此该战线消失，并入了秦氏工作室。

OP基金会曾编写著名文论《OP本法》。其最高领导层为2020年7月28日设立的OP议会，OP议会由部分加盟工作室领导人与部分外聘人员组成。OP直接控制组织为OP特别调查队，于2022年3月20日成立。OP基金会是有道小图灵网站记载数据中第四大组织。刚开始被FCX所管理，后来独立。

> “OP从不是VZH的敌人” —— 秦氏工作室室长John

OP基金会历代会长：

* 第一任：@秦氏工作室室长John
* 第二任：[@FCX会长-AOC黄子泓](https://icodeshequ.youdao.com/personal?userId=d3hvWFFVRGo3Njg5alpHSnA1T0dQbG9GWnJBQXdB)
* 第三任：@秦氏工作室室长John
* 第四任：[@qqcd](https://icodeshequ.youdao.com/personal?userId=cXFCNjBEODUwMDA1OEY5RkJFQTQxRDc2RTE3MTY3QzM3QQ%3D%3D)（明面会长）
 

### 历史小链接：《OP本法》完整内容
序：为减少有道社区抄袭现象，根据卡搭社区准则，制定本法。

1. 不得在改编他人作品下不标注原作者
2. 不得把别的网站的作品复制过来后不标注原网址
3. 任何复制过来的作品必须严格遵守《CC协议》
4. 任何反抄袭组织成员违法将严惩
5. 即日起，OP本法由OP基金会与FCX协会共同维护



[^1]: 2023年8月6日，@John发布[通告](https://icodeshequ.youdao.com/work/ee68608e93054c938c91a7d54e5d26db?from=discovery)，正式宣布YSTO解体。