﻿# 第1节：AOC原创者联盟

## AOC原创者联盟
AOC原创者联盟（英文全名：Alliance of Creators）于2021年1月25日由[秦氏工作室](https://icodeshequ.youdao.com/work/42b70e52eaf53fbc8ff2a4ee8cd111c7?from=discovery)前成员@冰然建立。由于当时反抄袭处于低谷期，AOC便顺势而起，打着“保护原创版权，打击抄袭势力”的口号复兴小图灵，并迅速取代了FCX的主导地位。

2021年3月，AOC打破FCX的80人记录，成为小图灵第一个成员数破百的组织，在建立中央委员会（后废除）后正式开启了辉煌繁荣的AOC时代。但后来据说因为@浅浅的原由，@冰然于2021年4月时辞去AOC会长一职，退出了AOC的事务。当时的副会长[@小星](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuMzY5ZmEyN2FkZTQyNGM5ZDlAMTYzLmNvbQ%3D%3D)成为AOC第二任会长，努力发展AOC并继续打击抄袭势力，@小星在位期间AOC发展强盛，社区稳定。同时因为AOC的成功导致的反抄袭组织创立热潮，著名的秦氏工作室第一次也是目前唯一一次接近解体。2021年6月，@冰然的妹妹@迷路的纯牛奶发动和平政变上位，当选AOC第三任会长，继续稳步发展AOC，同时[@AOC-有道J-酷燃](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuM2E1MmYxMGU1ODY0NDhmNzhAMTYzLmNvbQ%3D%3D)当选副会长。后@冰然于2021年8月回归，担任AOC第四任会长，开始了他的第二执政期，这段时期也是AOC的巅峰时期。

2021年10月，@冰然带着AOC官方账号未经通告就突然失踪，使AOC突然陷入危机，并进入其第一次大低谷时期，持续约220天左右的AOC时代正式结束，有道小图灵进入群雄时期。当时的副会长@AOC-有道J-酷燃任命高层成员@AOC-George为临时行政会长（第五任）以及一些临时管理人员，后@AOC-有道J-酷燃因为学业问题暂退，@AOC-George和临时管理人员们力挽狂澜，将AOC的影响力和权威性等都恢复了许多，但酷燃回归后与VZH发生冲突，使AOC和VZH的关系恶化。2022年1月12日@冰然回归，重新执政，使AOC脱离了低谷时期，可实力大不如前。

@冰然在AOC一周年前因为个人原因暂退，在qq发布公告宣布[@qqcd](https://icodeshequ.youdao.com/personal?userId=cXFCNjBEODUwMDA1OEY5RkJFQTQxRDc2RTE3MTY3QzM3QQ%3D%3D)为第七任会长（代理行政），后来@冰然再度回归执政一段时间后因为个人学业问题永退。@qqcd正式成为会长。由于各种原因，此时AOC成员零零散散，几乎名存实亡。2022年5月1日，会长@qqcd让位给@AOC-有道J-酷燃，经过多方会议，AOC全面重启，@冰然和@qqcd成为荣誉会长。2023年2月2日，@兔本兔豆奶成为第十一任会长，隔日，@AOC-有道J-酷燃再度成为会长，2023年2月4日，酷燃退站，AOC正式解散。

AOC原创者联盟于2021年2月至2022年1月左右为小图灵最大组织，鼎盛时期拥有138名实际掌权成员，记录一直保持至2022年3月6日。其是有道小图灵网站历史第二大组织，对有道小图灵网站反抄袭事业做出了极为重大的贡献。

AOC历代会长：

* 第一任：@冰然
* 第二任：[@小星](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuMzY5ZmEyN2FkZTQyNGM5ZDlAMTYzLmNvbQ%3D%3D)
* 第三任：@迷路的纯牛奶
* 第四任：@冰然
* 第五任：@AOC-George
* 第六任：@冰然
* 第七任：[@qqcd](https://icodeshequ.youdao.com/personal?userId=cXFCNjBEODUwMDA1OEY5RkJFQTQxRDc2RTE3MTY3QzM3QQ%3D%3D)
* 第八任：@冰然
* 第九任：[@qqcd](https://icodeshequ.youdao.com/personal?userId=cXFCNjBEODUwMDA1OEY5RkJFQTQxRDc2RTE3MTY3QzM3QQ%3D%3D)
* 第十任：[@酷燃](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuM2E1MmYxMGU1ODY0NDhmNzhAMTYzLmNvbQ%3D%3D)
* 第十一任：@兔本兔豆奶
* 第十二任：[@酷燃](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuM2E1MmYxMGU1ODY0NDhmNzhAMTYzLmNvbQ%3D%3D)


###历史小链接：UOC创作者联盟

2023年2月8日，前AOC高层@是ink不是link改名[@____](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuMWIwNTMyOWU2YjdiNDY3NDhAMTYzLmNvbQ%3D%3D)，重组AOC，改名为[UOC创作者联盟](https://icodeshequ.youdao.com/work/d5abc322889449658a1f67f40b33504f?from=discovery)（英文全名Union of Creators），但并未引起工作室创立热潮，且后来UOC渐渐荒废。4月1日，@____由于未知原因暂退，并将UOC交给[@John](https://icodeshequ.youdao.com/personal?userId=d3hvWFFVRGpfWkt1ZHRVVUtBR3RfbFZ1MnhMbFJF)管理，@John于4月2日改革UOC，4月4日UOC进入第二个发展期。后于2023年9月3日上午9时33分，@John宣布辞去UOC会长一职，并由[@DrEAmSkY梦辰](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuZGQ4ZTkwZmYwYmVjNGRhN2FAMTYzLmNvbQ%3D%3D)接替。