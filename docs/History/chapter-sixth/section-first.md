﻿# 第1节：VZH的加盟组织

## VZH的加盟组织

ZRR反抄袭突击队（英文全名：Zero Reality Anti-Plagiarism Rangers）在2021年3月21日就已经有了前身LIN工作室，创始人为[@LIN工作室室长上帝熊猫](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuOTRmZGZkZGUyNGIzNGNiODhAMTYzLmNvbQ%3D%3D)（后名@默龙）。其于2021年9月14日更名为AOD设计者联盟，后又于9月24日改为UTA协会，并于2021年12月5日15时6分正式改名为ZRR反抄袭突击队。ZRR的宗旨是改善小图灵社区风范，彻底根除抄袭势力。2021年12月23号，ZRR与VCU合并，VZH建立。后由于512章北海事件，ZRR最终改为ZYEF由[@梦辰](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuZGQ4ZTkwZmYwYmVjNGRhN2FAMTYzLmNvbQ%3D%3D)继续领导，最终ZYEF解体，改组为OWES协会。

VCU黑白会全名黑白无常反抄袭公会，创始人为@9360洛鹤。VCU曾虚构过其部分历史[^1]，2021年12月19日，VCU第三任会长@时七向抄袭势力宣战。2021年12月20日下午在外网，敌方公会大败黑白会，除了小图灵的分支其他全部解散。后小图灵分支解散，并入ZRR,ZRR后将其人员转让给XTW。

VCU黑白会的历代会长：

* 第一代（创始人）：@9360-洛鹤
* 第二代：@黑羽
* 第三代：@时七
* 第四代：[@默龙（临时行政会长）](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuOTRmZGZkZGUyNGIzNGNiODhAMTYzLmNvbQ%3D%3D)
* 第五代：@9360-洛鹤


XAA图灵反抄袭协会（英文名：XiaoTuLing Anti-Plagiarism  Association）是XTW图灵编程工作室的前身，由[@XAA-DreamSky](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuZGQ4ZTkwZmYwYmVjNGRhN2FAMTYzLmNvbQ%3D%3D)（现名@DrEAmSkY梦辰）成立于2022年2月20号上午，当时VCU和HMW解散后并入ZRR，使VZH人员比例严重失调，为了储存这些人员，@默麒找来@XAA-DreamSky，让其缔造了XAA协会。作为[@默麒](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuOTRmZGZkZGUyNGIzNGNiODhAMTYzLmNvbQ%3D%3D)创立的首个傀儡协会。XAA继承了VCU和HMW的人员(去除了抄袭成员)，于建立当日加入VZH。512事件后去除反抄袭性质，改组为XTW工作室。
 

[^1]:“VCU黑白会于2017年4月7日建立于外网，外网人数最高时达1.7万，自从第二任继位后开始走下坡路，最低达到3.1千人左右，2020年初，第三任会长继位，并进入小图灵社区，此时人数为5.8千”的这段VCU历史属创始人虚构，真正的黑白会早就于2014年解散，就算是鼎盛时期也只有1000人。 