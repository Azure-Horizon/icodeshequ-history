﻿# 第3节：纳米工作室

## 纳米工作室

纳米工作室于2019年5月1日成立（存疑，为[@科尼塞克](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuNThiNzI3OTNjMDRiNDQ2NDhAMTYzLmNvbQ%3D%3D)独自说法，其多次修改成立日期），此后两年里，纳米虽然没有什么名气，但也涌现了许多出色的作品。2020年5月31日，纳米工作室位于编程猫社区的总部帅哥工作室成立，至今仍旧存在，并仍旧由@夙愿领导，其共有50余人。

2021年AOC成立后，纳米工作室与其合作。2021年7月3日，@夙愿被曝抄袭和仿冒AOC室长@冰然后正式退站，@科尼塞克成为纳米工作室室长，此后纳米进入巅峰时期，涌现出许多优秀人才和好作，工作室欣欣向荣。

2021年12月24号晚上8点，纳米-盾（纳米精英部门）通过加入ZRR反抄袭突击队的ZRRP议会成为VZH成员。2022年1月18日14时29分，纳米全体加入，[@科尼塞克](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuNThiNzI3OTNjMDRiNDQ2NDhAMTYzLmNvbQ%3D%3D)由于暂退，便将纳米托付于[@默麒](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuOTRmZGZkZGUyNGIzNGNiODhAMTYzLmNvbQ%3D%3D)，双方签署了《纳米加入VZH条约》，制定了@默麒领导纳米反抄袭工作室的中央准则，当日15时整，@默麒全面接手纳米。

2022年2月1日，第一次纳米独立运动爆发，由@科尼塞克和@纳米工作室官方领导的独立派与@默麒领导的VZH派爆发冲突，后期@科尼塞克与@默麒达成《纳米协议》，约定一旦创始人@夙愿发布作品宣告回归，@默麒必须立刻让出大权，并宣布纳米独立，否则纳米无权独立，无权逼迫@默麒卸任纳米工作室室长一职或给社区造成动乱。

2月7日11时，第二次纳米独立运动爆发，新加入的纳米成员，同时也是当时社区著名人物的[@潘西](https://icodeshequ.youdao.com/personal?userId=d3hvWFFVRGo3RUFjMHg1TV85dUdTQjdHa0dlR0ZVXzI%3D)和[@科尼塞克](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuNThiNzI3OTNjMDRiNDQ2NDhAMTYzLmNvbQ%3D%3D)共同领导的回归派与@默麒的VZH派爆发正面冲突，评论区骂战连天。@科尼赛克撕毁《纳米协议》（@潘西不知道《纳米协议》），@潘西要求[@默麒](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuOTRmZGZkZGUyNGIzNGNiODhAMTYzLmNvbQ%3D%3D)让出大权，@默麒不同意，并曝光多条证据，认为纳米方无理。后期由于纳米得到小图灵众多社区成员的支持，且@默麒败诉，因此@默麒于2月8日宣告纳米独立。2022年3月6日，@科尼赛克同VZH方谈话后，纳米返回VZH，采用独立自治。

2022年5月10日，纳米反抄袭工作室以独立工作室的身份参加了第二次小图灵全体大会，但大会因512网警事件而推迟。在512网警事件爆发后，由于理性的克制，纳米工作室没有受到任何波及，并于5月15日再次参加了推迟的大会。6至7月，纳米先后成为ISP议会和TAC联邦的成员，之后逐渐衰弱，纳米盾和议会衰败解散。@科尼赛克与@潘西产生矛盾，纳米工作室分裂。7月17日，@潘西代表所有纳米类组织（存疑）宣布正式终止条约并独立，在一个月后，所有纳米组织解体。8月中旬，纳米公社成立，实施利刃计划改革，但却以执行官@曜熙（即@默麒）的退出而告终，纳米公社不了了之。

9月初，双子工作室成立，并宣布加入TAC，后TAC解散，又成为ZYEF组织成员，最后消失。2023年6月20日，纳米工作室卷土重来。再次执行利刃计划第四版决议，并进入蓬勃发展期。后2023年9月1日，由于个人原因，@科尼赛克退站，@王堇涵成为纳米工作室室长，并再度发展纳米。