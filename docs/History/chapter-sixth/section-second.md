﻿# 第2节：CPA社区净化协会

## CPA社区净化协会

CPA社区净化协会英文全称为Community purification Association。由[@qqcd](https://icodeshequ.youdao.com/personal?userId=cXFCNjBEODUwMDA1OEY5RkJFQTQxRDc2RTE3MTY3QzM3QQ%3D%3D)在2021年10月25日创立，具有十分大的影响力。CPA的创立目的是从各方面管制小图灵的抄袭现象，并与其他协会携手保护小图灵。

CPA成立之前，小图灵抄袭泛滥，为了改变这种情况，当时AOC会长@冰然努力扩大反抄袭势力、宣传反抄袭思想，但这只招来了一堆混日子的垃圾成员，@冰然也没有作出有效措施来改变（《图灵维基》说法），最终使AOC内部成员水分很高，甚至出现内部抄袭情况。CPA创建后，因当时@qqcd名声显赫且反抄袭情况不容乐观,所以得到包括当时AOC副会长[@酷燃](https://icodeshequ.youdao.com/personal?userId=dXJzLXBob25leWQuM2E1MmYxMGU1ODY0NDhmNzhAMTYzLmNvbQ%3D%3D)在内的众人的广泛支持。

CPA创立后，@qqcd将CPA的加入人员分为三批，分别为督察组，宣传组和程序组。督察组专门整治抄袭现象；宣传组专门宣传CPA和反抄袭思想，而程序组负责用心做原创作品，改变社区创作环境。CPA成立之初，[@qqcd](https://icodeshequ.youdao.com/personal?userId=cXFCNjBEODUwMDA1OEY5RkJFQTQxRDc2RTE3MTY3QzM3QQ%3D%3D)命令宣传组大力宣传CPA和反抄袭思想，尽力地将反抄袭思想渗透给小图灵的新用户，扩大CPA和反抄袭组织的势力与影响力。@qqcd还让督察组在监督反抄袭时，以监督高赞、影响力大的作品和用户为先，后来还设立“CPA整治专栏”来挂人。@qqcd采取的措施颇有成效，在短短三个月内，CPA使用网络暴力的方式歼灭了十余个抄袭作品，成功消灭那些“抄袭势力的领头羊”。然而CPA主要使用网暴方式（骂战、刷屏、逼迫）处理抄袭，而这大有问题。曾有些人指出了CPA的错误，但@qqcd不以为然。

2022年初期，社区的抄袭现象被各种反抄袭组织所压制，其中CPA功不可没，整个小图灵繁荣昌盛，因此CPA的作用渐渐不再明显，逐渐淡出了人们的视野。2022年上半年，VZH势力逐渐强大，为了压制VZH，OP被秦氏工作室室长[@John](https://icodeshequ.youdao.com/personal?userId=d3hvWFFVRGpfWkt1ZHRVVUtBR3RfbFZ1MnhMbFJF)重启。2022年3月26日，@John提出OP和CPA合并的请求,表示这样可以间接削弱VZH的势力，防止其“控制整个小图灵”，并能给CPA带来新鲜血液。@qqcd最终答应，成为了合并OP后CPA的明面会长（实际由@John操控）。

2022年5月11日，512章北海事件发生，以[@qqcd](https://icodeshequ.youdao.com/personal?userId=cXFCNjBEODUwMDA1OEY5RkJFQTQxRDc2RTE3MTY3QzM3QQ%3D%3D)为首的OP、CPA军团和VZH军团浩浩荡荡网暴章北海，后网信办介入，@qqcd被封号，CPA也因为此次事件而宣布解散。